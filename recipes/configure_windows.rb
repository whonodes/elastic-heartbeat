#
# Cookbook:: elastic-heartbeat
# Recipe:: configure_windows
#
# Copyright:: 2019, The Authors, All Rights Reserved.

directory 'C:\Program Files\Heartbeat' do
  action :create
end