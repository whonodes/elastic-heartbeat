#
# Cookbook:: elastic-heartbeat
# Recipe:: configure_unix
#
# Copyright:: 2019, The Authors, All Rights Reserved.

directory '/etc/heartbeat' do
  action :create
  owner 'root'
  group 'root'
  mode '0755'
end

directory '/usr/share/heartbeat' do
  action :create
  owner 'root'
  group 'root'
  mode '0755'
end

directory '/var/lib/heartbeat' do
  action :create
  owner 'root'
  group 'root'
  mode '0755'
end

directory '/var/log/heartbeat' do
  action :create
  group 'root'
  owner 'root'
  mode '0755'
end

template '/etc/heartbeat/heartbeat.yml' do
  action :create
  owner 'root'
  group 'root'
  mode '0754'
  source 'heartbeat.yml.erb'
  notifies :restart, 'service[heartbeat-elastic]', :delayed
end