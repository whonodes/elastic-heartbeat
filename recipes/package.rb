package 'heartbeat-elastic' do
  action node['elastic']['heartbeat']['package'].to_sym
  notifies :restart, 'service[heartbeat-elastic]', :delayed
end
