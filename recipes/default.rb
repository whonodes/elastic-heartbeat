#
# Cookbook:: heartbeat
# Recipe:: default
#
# Copyright:: 2019, The Authors, All Rights Reserved.
include_recipe 'elastic-repo::default'

include_recipe 'elastic-heartbeat::package'
include_recipe 'elastic-heartbeat::service'
include_recipe 'elastic-heartbeat::configure_unix'