# Manages the heartbeat service

service 'heartbeat-elastic' do
  action [node['elastic']['heartbeat']['state'].to_sym, node['elastic']['heartbeat']['enabled'].to_sym]
end
