# InSpec test for recipe heartbeat::package

# The InSpec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

describe package('heartbeat-elastic') do
  it { should be_installed }
end