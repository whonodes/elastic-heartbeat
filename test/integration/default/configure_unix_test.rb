# InSpec test for recipe elastic-heartbeat::configure_unix

# The InSpec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

return if os.windows?

describe directory('/etc/heartbeat') do
  it { should exist }
  its('group') { should eq 'root' }
  its('owner') { should eq 'root' }
  its('mode') { should cmp '0755' }
end

describe directory('/var/lib/heartbeat') do
  it { should exist }
  its('group') { should eq 'root' }
  its('owner') { should eq 'root'}
end

describe directory('/usr/share/heartbeat') do
  it { should exist }
  its('group') { should eq 'root' }
  its('owner') { should eq 'root' }
  its('mode') { should cmp '0755' }
end

describe directory('/var/log/heartbeat') do
  it { should exist }
  its('group') { should eq 'root' }
  its('owner') { should eq 'root' }
  its('mode') { should cmp '0755' }
end

describe file('/etc/heartbeat/heartbeat.yml') do
  it { should exist }
  its('group') { should eq 'root' }
  its('owner') { should eq 'root' }
  its('mode') { should cmp '0754'}
end