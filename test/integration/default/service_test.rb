# InSpec test for recipe heartbeat::service

# The InSpec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

describe service('heartbeat-elastic') do
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
end