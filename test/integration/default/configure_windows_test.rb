# InSpec test for recipe elastic-heartbeat::configure_windows

# The InSpec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

return unless os.windows?

describe directory('C:\Program Files\Heartbeat') do
  it { should exist }
end
