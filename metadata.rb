name 'elastic-heartbeat'
maintainer 'Matthew Iverson'
maintainer_email 'matthewdiverson@gmail.com'
license 'MIT'
description 'Installs/Configures heartbeat'
long_description 'Installs/Configures heartbeat'
version '0.1.1'
chef_version '>= 14.0'

%w(centos fedora debian redhat ubuntu).each do |os|
  supports os
end

depends 'elastic-repo'

issues_url 'https://bitbucket.org/whonodes/elastic-heartbeat/issues'
source_url 'https://bitbucket.org/whonodes/elastic-heartbear/src'
